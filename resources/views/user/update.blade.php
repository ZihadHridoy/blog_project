@extends ('layouts.master')

@section ('content')

<div class="col-sm-8 blog-main">

	<div class="row" style="text-align: center;">
		<div class="col-md-12">
			<h1><u>Update Your Profile</u></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<form method="POST" action="/update_profile">

				  {{ csrf_field() }}
				  <input type="hidden" name="_method" value="PUT">

				  <div class="form-group">
				    <label for="name">Name:</label>
				    <input type="text" class="form-control" name="name" style="border: 1px solid #428bca;" value="{{ Auth::user()->name }}">
				  </div>

				  <div class="form-group">
				    <label for="email">Email:</label>
				    <input type="email" name="email" class="form-control" style="border: 1px solid #428bca;" value="{{ Auth::user()->email }}">
				  </div>

				  <div class="form-group">
				    <label for="image">Photo :</label>
				    <input type="file" name="image">
				  </div>

				  <button type="submit" class="btn btn-primary">Update</button>

				</form>	
		</div>
	</div>

	
</div><!-- /.blog-main -->

@endsection