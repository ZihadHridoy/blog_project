<?php


Route::get('/', 'PostsController@index')->name('home');

Route::get('/posts/create', 'PostsController@create');

Route::post('/posts', 'PostsController@store');

Route::get('/posts/{post}', 'PostsController@show');

Route::get('/posts/{post}/edit', 'PostsController@edit');

Route::put('/posts/{post}/update', 'PostsController@update');

Route::get('/posts/tags/{tag}', 'TagsController@index');

Route::post('/posts/{post}/comments', 'CommentsController@store');

Route::get('/register', 'Registrationcontroller@create');

Route::post('/register', 'Registrationcontroller@store');

Route::get('/login', 'Sessioncontroller@create');

//Route::post('/login', 'SessionController@store');

Route::post('login', [ 'as' => 'login', 'uses' => 'SessionController@store']);

Route::get('/logout', 'SessionController@destroy');

Route::get('/profile', 'UserController@index');

Route::get('/update_profile', 'UserController@update');

Route::post('/update_profile', 'UserController@update');


Route::get('/file_upload', 'FileController@index')->name('file.upload');

Route::post('/file_upload', 'FileController@store');



